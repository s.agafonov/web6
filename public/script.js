function updatePrice() {
    let s = document.getElementsByName("prodType");
    let select = s[0];
    let price = 0;
    let prices = getPrices();
    let priceIndex = parseInt(select.value) - 1;
    if (priceIndex >= 0) {
        price = prices.prodTypes[priceIndex];
    }
    
    let radioDiv = document.getElementById("radios");
    radioDiv.style.display = (select.value == "2" ? "block" : "none");
    
    let radios = document.getElementsByName("prodOptions");
    radios.forEach(function(radio) {
      if (radio.checked) {
        let optionPrice = prices.prodOptions[radio.value];
        if (optionPrice !== undefined) {
            price += optionPrice;
        }
    }
    });
  
    let checkDiv = document.getElementById("checkbox");
    checkDiv.style.display = (select.value == "3" ? "block" : "none");

    let checkbox = document.querySelectorAll("#checkbox input");
    checkbox.forEach(function(checkbox) {
      if (checkbox.checked) {
        let propPrice = prices.prodProperties[checkbox.name];
        if (propPrice !== undefined) {
          price += propPrice;
        }
    }
    });
    
    let prodPrice = document.getElementById("prodPrice");
    let amount = document.getElementById("amount").value;
    prodPrice.innerHTML = price * amount + " руб.";
}
  
function getPrices() {
    return {
    prodTypes: [2000, 20000, 30000],
    prodOptions: {
        option1: 0,
        option2: 10000,
        option3: 20000,
    },
    prodProperties: {
        prop: 10000,
    }
    };
}
  
window.addEventListener('DOMContentLoaded', function (event) {
    let radioDiv = document.getElementById("radios");
    radioDiv.style.display = "none";
    
    let s = document.getElementsByName("prodType");
    let select = s[0];
    select.addEventListener("change", function(event) {
        let target = event.target;
        console.log(target.value);
        updatePrice();
    });
     
    let radios = document.getElementsByName("prodOptions");
    radios.forEach(function(radio) {
        radio.addEventListener("change", function(event) {
        let r = event.target;
        console.log(r.value);
        updatePrice();
    });
    });
  
    let checkbox = document.querySelectorAll("#checkbox input");
    checkbox.forEach(function(checkbox) {
        checkbox.addEventListener("change", function(event) {
        let c = event.target;
        console.log(c.name);
        console.log(c.value);
        updatePrice();
    });
    });
    updatePrice();
});
